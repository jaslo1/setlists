Vasona Vibrations
2013-06-29 (?)

  5 - Oh, You Beautiful Doll - 134
 35 - Basie Straight Ahead - 180
  6 - Getting Sentimental Over You
 71 - Big Noise from Winnetka - 175
  ? - Fly Me to The Moon (vocal)
 18 - Moten Swing - 135-140
 50 - Jalousie
  9 - Clear Day
 21 - Somewhere over the Rainbow
 72 - Sun Valley Jump  - 160
 43 - East of the Sun
  3 - Leap Frog - 177
  ? - Dancing Cheek to Cheek (vocal)
188 - Sweet Georgia Brown

 11 - Little Brown Jug - 145
 32 - That Old Black Magic - 130
266 - I Hadn't Anyone
233 - Cottontail
 66 - Someone to Watch Over Me
105 - 920 Special - 160
 95 - Who's Sorry Now? - 155
 97 - Besame Mucho
  1 - In the Mood - 175
 94 - As Long as I Live
 86 - After the Lovin'
270 - American Patrol - 175
  ? - All of Me (vocal)
 25 - When You're Smiling
 13 - Opus One - 160

Cut:
243 - Dateline Newport
 78 - Don't Worry About Me
738 - Georgia

